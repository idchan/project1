/*
  Project 1
  Name of Project: Way to Faker
  Author: 20150357 ChanHyung Park
  Date: 20200605
*/


// Globals
String screen;
color yellow = color(255,221,41);
color brown = color(81,63,0);
color green = color(8,62,68);
color gray2 = color(196);
color gray1 = color(242);
color white = color(253);
color red = color(235,87,87);
color blue = color(118,176, 255);
int titlesize, middlesize, subsize, enemydelay, highscore, enemyterm;
PFont font;
PImage map;
Button start, back, setting, controls, restart, play, menu;
ArrayList<Enemy> Enemys = new ArrayList<Enemy>();
float mespeed, enemyspeed, qspeed, ghostspeed;
PVector v1, maindirection;
Maincharacter me;


/********* SETUP BLOCK *********/
void setup() {
  size(1200, 800);
  screen = "Home";
  titlesize = 80;
  middlesize = 40;
  me = new Maincharacter();
  subsize = 32;
  mespeed = 15;
  enemyspeed = mespeed*2/3;
  qspeed = 50;
  highscore = 0;
  enemyterm = 0;
  font = loadFont("Roboto-Black-48.vlw");
  textFont(font);
  textAlign(CENTER,CENTER);
  v1 = new PVector(0,0);
  map = loadImage("ingamemap.png");
  ellipseMode(CENTER);
  // set buttons
  start = new Button("START",middlesize, 687,272,372,89,20,gray2);
  restart = new Button("RESTART",middlesize, 687,272,372,89,20,gray2);
  setting = new Button("SETTING",middlesize,687,560,372,89,20,gray2);
  menu = new Button("MENU",middlesize,687,416,372,89,20,gray2);
  controls = new Button("CONTROLS",middlesize,687,416,372,89,20,gray2);
  back = new Button("BACK",subsize,-20,106,240,71,20,gray2);
}


/********* DRAW BLOCK *********/
void draw() {
  Homescreen();
  if (screen.equals("Home") == true){
    Homescreen();
  }
  else if (screen.equals("Setting") == true){
    Settingscreen();
  }
  else if (screen.equals("Controls") == true){
    Controlscreen();
  }
  else if (screen.equals("Ingame") == true){
    Ingamescreen();
  }
  else if (screen.equals("Gameover") == true){
    Gameoverscreen();
  }
  println(Enemys);
}

/********* PAGE CONTENTS *********/
void Homescreen(){
  background(green);
  //Title
  textSize(titlesize);
  fill(brown);
  text("WAY TO FAKER", width/2, 155);
  fill(yellow);
  text("WAY TO FAKER", width/2, 145);
  //buttons
  start.ismouseOver();
  start.drawButton();
  //setting.ismouseOver();
  //setting.drawButton();
  controls.ismouseOver();
  controls.drawButton();
  
  // highscore
  fill(gray1);
  rect(155,272,372,377,20);
  fill(gray2);
  rect(155,272,372,89,20,20,0,0);
  fill(gray2);
  fill(0);
  textSize(middlesize);
  text("HIGHSCORE", 155+186,293+24);
  textSize(titlesize);
  text(highscore,155+186,482);
}
void Settingscreen(){
  background(green);
  //Title
  textSize(titlesize);
  fill(brown);
  text("SETTING", width/2, 155);
  fill(yellow);
  text("SETTING", width/2, 145);
  //buttons
  back.ismouseOver();
  back.drawButton();
}
void Controlscreen(){
  background(green);
  //Title
  textSize(titlesize);
  fill(brown);
  text("CONTROLS", width/2, 155);
  fill(yellow);
  text("CONTROLS", width/2, 145);
  //buttons
  back.ismouseOver();
  back.drawButton();
  
  fill(gray1);
  rect(155,272,890,377,20);
  textSize(subsize);
  textAlign(LEFT,TOP);
  fill(0);
  text("F=Flash",235,354);
  text("Q=Skill shot",235,442);
  text("RIGHT CLICK = Moving",235,530);
  text("E=Vision shift",680,354);
  text("S=Stop",680,442);
  textAlign(CENTER,CENTER);
}
void Gameoverscreen(){
  background(green);
  //Title
  textSize(titlesize);
  fill(brown);
  text("GAMEOVER", width/2, 155);
  fill(yellow);
  text("GAMEOVER", width/2, 145);
  
  //score
  fill(gray1);
  rect(155,272,372,377,20);
  fill(gray2);
  rect(155,272,372,89,20,20,0,0);
  fill(gray2);
  fill(0);
  textSize(middlesize);
  text("SCORE", 155+186,293+24);
  textSize(titlesize);
  text(me.score,155+186,482); 
  //set highscore
  if(me.score>highscore) highscore = me.score;
  //Buttons
  restart.ismouseOver();
  restart.drawButton();
  menu.ismouseOver();
  menu.drawButton();
}

void Ingamescreen() {
  image(map, 0, 0,width,height);
  // Score
  textSize(middlesize);
  fill(white);
  text(me.score, width/2, 68);
  
  // skills cooltime graph
  drawGraph(me.qcool, 80,50);
  drawGraph(me.fcool,80,95);
  drawGraph(me.ecool,80,140);
  textSize(subsize);
  fill(white);
  text("Q",320,64);
  text("F",320,109);
  text("E",320,154);
  
  // create & update maincharacter
  me.update();
  me.display();
  
  // create enemies
  if(Enemys.size() == 0) Enemybuilder();
  else{
   Enemybuilder();
   Enemybuilder();
  }
  for(Enemy b: Enemys){
    if(b.visible==true){
        b.setDirection();
        b.update();
        b.display();
    }
  }
}

/********* Classes *********/

void drawGraph(int cooltime, int x, int y){
  if(cooltime == 100){
      fill(white);
      rect(x,y,cooltime*2,30);
    }
    else if(cooltime >70){
      fill(yellow);
      rect(x,y,cooltime*2,30);
    }
    else{
      fill(red);
      rect(x,y,cooltime*2,30);
    }
}

class Button
{
  protected int x, y,w,h,r;
  public color c;
  protected String text;
  protected int fs;
  Button(String text, int fontsize, int x, int y, int width, int height, int radius, color col){
    this.text = text;
    this.x = x;
    this.y = y;
    w = width;
    h = height;
    r = radius;
    c = col;
    fs = fontsize;
  }
  void drawButton(){
    noStroke();
    rectMode(CORNER);
    fill(c);
    rect(x,y,w,h,r);
    fill(0);
    textSize(fs);
    text(text, x+w/2, y+h/2);
    //to be modified
  }
  void ismouseOver(){ // some button does not hvae mouse over effect
    if(mouseX>=x && mouseX<=x+w){
      if(mouseY>=y && mouseY<=y+h){
        c = yellow;
      }
      else c = gray2;
    }
    else c = gray2;
  }
  boolean  ismouseClicked(){
    if(mouseX>=x && mouseX<=x+w){
      if(mouseY>=y && mouseY<=y+h){
        return true;
      }
      else return false;
    }
    else c = gray2;
    return false;
  }
}

abstract class Character{
  protected float xp, yp;
  protected float xv, yv;
  protected PVector directionvector;
  protected float speed;
  Character(float x, float y,float speed, PVector directionvector){
    xp = x;
    yp = y;
    this.directionvector = directionvector;
    //this.directionvector = directionvector.normalize();
    this.speed = speed;
    xv =0;
    yv =0;
  }
  abstract void display();
  void update(){
    xp += xv;
    yp += yv;
  }
  void setDirection(PVector direction, float speed){
     directionvector = direction.normalize();
     this.speed = speed;
     xv = speed*directionvector.x;
     yv = speed*directionvector.y;
  }
  void setVelocity(float speed){
     this.speed = speed;
     xv = speed*directionvector.x;
     yv = speed*directionvector.y;
  }
  abstract void reset();
}

class Maincharacter extends Character{  
  int qcool, ecool,fcool, score;
  boolean ghost;
  PVector terminal;
  private ArrayList<Q> Qs = new ArrayList<Q>();
  Maincharacter(){
    super(width/2,height/2, mespeed,v1); 
    qcool = 100;
    ecool = 100;
    fcool = 100;
    ghost = false;
    score = 0;
  }
  void display(){
    
    for(Q b: Qs){
      if(b.visible == true){
        fill(white);
        ellipse(b.loc.x,b.loc.y,20,20);
      }
    }
    println(speed);
    if(ghost == true){//main character
      fill(blue);
    }
    else{
      fill(yellow);                           
    }
    ellipse(xp,yp,50,50);
  }
  void reset(){
    score = 0;
    xp = width/2;
    yp = height/2;
    xv = 0;
    yv = 0;
    directionvector = v1;    
  }
  void setDirection(PVector direction, float speed){
     directionvector = direction.normalize();
     this.speed = speed;
     terminal = new PVector(mouseX,mouseY);
     xv = speed*directionvector.x;
     yv = speed*directionvector.y;
  }
  void shootQ(PVector direction){
    PVector location;
    location = new PVector(xp,yp);
    Qs.add(new Q(location,direction));
  }
  void ghostE(){
    ghost = true;
  }
  void flashF(PVector direction){
    xp = xp + direction.normalize().x*150.0;
    yp = yp + direction.normalize().y*150.0;
    PVector newdirection;
    newdirection = new PVector(terminal.x-me.xp,terminal.y-me.yp);
    xv = speed*newdirection.normalize().x;
    yv = speed*newdirection.normalize().y;
  }
  void update(){
    if(terminal == null){
    }
    else{
      if((terminal.x-xp)*(terminal.x-xp-xv)>0){
        if(ghost == false) xp += xv;
        else xp+=2*xv;
        if(xp>1175) xp = 1175;
        if(xp<25) xp = 25;
        if(ghost==false) yp += yv;
        else yp+=2*yv;
        if(yp>775) yp = 775;
        if(yp<25) yp = 25;
      }
      else{
        xp = terminal.x;
        yp = terminal.y;
      }
      if(Qs.size()>0){
        qcool+=10;
        for(Q b: Qs){                               // skill Q location
          if(b.visible == true){
            b.loc.x= b.loc.x+b.speed*b.direction.x;
            b.loc.y= b.loc.y+b.speed*b.direction.y;
          }
          if(qcool>=80){
            b.visible = false;
          }
        }
        
        if(qcool >95){       // qcool setting
          qcool = 100;
        }
        if(qcool == 100){
          Qs.clear();
        }
      }
      if(fcool<100){
        fcool+=2;
      }
      if(ecool<100){
        ecool+=2;
        println(2);
        if(ecool ==60){
          ghost = false;
          setVelocity(mespeed);
        }
      }
      
    }
  }
}

class Enemy extends Character{
  boolean visible;
  Enemy(float x, float y){
    super(x,y, enemyspeed,v1);
    PVector direction;
    direction = new PVector(me.xp-x,me.yp-y);
    super.directionvector = direction;
    visible = true;
  }
  void display(){
    fill(gray2);                
    ellipse(xp,yp,50,50);
  }
  void reset(){
  }
  void setDirection(){
    PVector direction;
    direction = new PVector(me.xp-xp,me.yp-yp);
    super.setDirection(direction, enemyspeed);
  }
  void update(){
    xp += xv;
    yp += yv;
    this.catchme();
    this.beattacked();
  }
  void catchme(){
    if(((me.xp-xp)*(me.xp-xp)+(me.yp-yp)*(me.yp-yp))<2500) screen = "Gameover";
  }
  void getscore(){
    textSize(subsize);
    fill(yellow);
    text("+10",xp,yp);
  }
  void beattacked(){
    for(Q b: me.Qs){
      if(b.visible == true){
        if(((b.loc.x-xp)*(b.loc.x-xp)+(b.loc.y-yp)*(b.loc.y-yp))<1225) {
          visible = false;
          me.score +=10;
        }
      }
    }
  }
}

class Q
{
  private PVector loc;
  private PVector direction;
  private float speed;
  private boolean visible;
  Q(PVector loc, PVector direction){
    this.loc = loc;
    this.direction = direction.normalize();
    visible = true;
    speed = qspeed;
  }
}

void Enemybuilder(){
  if(enemyterm > 100){
    float x,y;
    x=y=0;
    Enemy A;
    int i = (int)random(0, 7);
    switch(i) {
    case 0: 
      x = -50.0;
      y = -50.0;
      break;
    case 1: 
      x = width/2;
      y = -50.0;
      break;
    case 2: 
      x = width+50.0;
      y = -50.0;
      break;
    case 3: 
      x = width+50.0;
      y = height/2;
      break;
    case 4: 
      x = width+50.0;
      y = height+50.0;
      break;
    case 5: 
      x = width/2;
      y = height+50.0;
      break;
    case 6: 
      x = -50.0;
      y = height+50.0;
      break;
    case 7: 
      x = -50.0;
      y = height/2;
      break;
    }
    A = new Enemy(x,y);
    Enemys.add(A);
    enemyterm = 0;
  }
  else enemyterm += 3;
}

void newGame(){
  me.reset();
  enemyterm = 0;
  Enemys.clear();
}

/********* Mouse Action *********/

void mousePressed() {
  if (screen.equals("Home") == true){
      //start.ismouseClicked();
      if (setting.ismouseClicked()) screen = "Setting";
      else if(controls.ismouseClicked()) screen = "Controls";
      else if(start.ismouseClicked()) 
        {
          screen = "Ingame";
        }
  }
  else if (screen.equals("Setting") == true){
    if (back.ismouseClicked()) screen = "Home";
  }
  else if (screen.equals("Controls") == true){
    if (back.ismouseClicked()) screen = "Home";
  }
  else if (screen.equals("Ingame") == true){
    maindirection = new PVector(mouseX-me.xp,mouseY-me.yp);
    me.setDirection(maindirection, mespeed);
  }
  else if (screen.equals("Gameover") == true){
      if (restart.ismouseClicked()) {
        newGame();
        screen = "Ingame";
      }
      else if(menu.ismouseClicked()) screen = "Home";
  }
}

/********* Keyboard Action *********/
void keyPressed() {
  //if(me.qcool == 100){
  //  me.shootQ(direction);
  //}
  if(me.terminal != null){
    if (keyPressed) {
      PVector direction;
        direction = new PVector(mouseX-me.xp,mouseY-me.yp);
      if (key == 'q' || key == 'Q') {
        if(me.qcool == 100){
          me.shootQ(direction);
          me.qcool = 0;
        }
      }
      if (key == 'f' || key == 'F') {
        if(me.fcool == 100){
          me.flashF(direction);
          me.fcool = 0;
        }
      }
      if (key == 'q' || key == 'Q') {
        if(me.qcool == 100){
          me.shootQ(direction);
          me.qcool = 0;
        }
      }if (key == 'e' || key == 'e') {
        if(me.ecool == 100){
          me.ghostE();
          me.ecool = 0;
        }
      }
      if (key == 'S' || key == 's') {
        me.terminal.x = me.xp;
        me.terminal.y = me.yp;
      }
    } 
  }
}
